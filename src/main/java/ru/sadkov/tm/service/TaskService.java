package ru.sadkov.tm.service;

import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.repository.TaskRepository;
import ru.sadkov.tm.util.RandomUUID;

public class TaskService {
    private TaskRepository taskRepo = new TaskRepository();

    public void saveTask(String taskName) {
        if(taskName==null||taskName.isEmpty()){
            System.out.println("[INCORRECT NAME]");
        }else{
            taskRepo.saveTask(new Task(taskName, RandomUUID.genRandomUUID()));
            System.out.println("[OK]");
        }
    }

    public void removeTask(String taskName) {
        if(taskName==null||taskName.isEmpty()){
            System.out.println("[INCORRECT NAME]");
        }else{
            taskRepo.removeTask(taskName);
            System.out.println("[OK]");
        }
    }

    public void showTasks() {
        if(taskRepo.isEmpty()){
            System.out.println("[NO TASKS]");
        }else {
            int i =1;
            for (Task task:taskRepo.getTaskList()) {
                System.out.println(i+". "+task.getTaskName());
            }
        }
    }

    public void clearTasks() {
        taskRepo.clearTasks();
    }
}
