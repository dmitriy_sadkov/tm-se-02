package ru.sadkov.tm.service;

import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.repository.ProjectRepository;
import ru.sadkov.tm.util.RandomUUID;

public class ProjectServise {
    private ProjectRepository projectRepo = new ProjectRepository();


    public void saveProject(String projectName) {
        if(projectName==null||projectName.isEmpty()){
            System.out.println("[INCORRECT NAME]");
        }else{
            projectRepo.saveProject(new Project(projectName, RandomUUID.genRandomUUID()));
            System.out.println("[OK]");
        }
    }

    public void removeProject(String projectName) {
        if(projectName==null||projectName.isEmpty()){
            System.out.println("[INCORRECT NAME]");
        }else{
            projectRepo.removeProject(projectName);
            System.out.println("[OK]");
        }
    }

    public void showProjects() {
        if(projectRepo.isEmpty()){
            System.out.println("[NO PROJECTS]");
        }else {
            // System.out.println("[PROJECT LIST]");
            int i =1;
            for (Project project:projectRepo.getProjectList()) {
                System.out.println(i+". "+project.getProjectName());
            }
        }
    }

    public void clearProjects() {
        projectRepo.clearProjects();
    }
}
